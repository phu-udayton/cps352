$l = 10;
$d = 10;
# reads an integer from standard input
$input = <STDIN>;
if ($input == 5) {
   print "Before the call to sub1 -- l: $l, d: $d\n";
   &sub1();
   print "After the call to sub1 --  l: $l, d: $d\n";
} else {
    print "Before the call to sub2 -- l: $l, d: $d\n";
    &sub2();
    print "After the call to sub2 --  l: $l, d: $d\n";
   }
exit(0);

sub sub1 {
   my $l; # only in this block (statically scoped)
   local $d; # accessible to children (dynamically scoped)
   $l = 5;
   $d = 20;
   print "Inside the call to sub1 -- l: $l, d: $d\n";
   print "Before the call to sub2 -- l: $l, d: $d\n";
   &sub2();
   print "After the call to sub2 --  l: $l, d: $d\n";
}
sub sub2 {
   print "Inside the call to sub2 -- l: $l, d: $d\n";
}